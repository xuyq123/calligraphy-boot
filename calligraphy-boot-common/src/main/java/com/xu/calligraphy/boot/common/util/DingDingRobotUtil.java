package com.xu.calligraphy.boot.common.util;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.CharsetUtils;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xu
 * @date 2021/7/28 15:49
 * 钉钉机器人 https://ding-doc.dingtalk.com/doc#/serverapi2/qf2nxq
 */
@Component
public class DingDingRobotUtil {

    private static final Logger logger = LoggerFactory.getLogger(DingDingRobotUtil.class);

    @Value("${dingding.robotUrl}")
    private String robotUrl;

    @Value("${dingding.signSecret}")
    private String signSecret;

    @Value("${dingding.isSendMsg}")
    private String isSendMsg;

    public String sendDingDingRobotMessage(String message) {
        if ("0".equals(isSendMsg)) {
            return "";
        }
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String responseInfo = null;
        try {
            //robotUrl = robotUrl == null ? "https://oapi.dingtalk.com/robot/send?access_token=95e470ec509931a88807c6d052583385e15555ed3e66b23ecc5e54e2c76d4365" : robotUrl;
            StringBuffer url = new StringBuffer();
            Long timestamp = System.currentTimeMillis();
            url.append(robotUrl).append("&timestamp=").append(timestamp).append("&sign=").append(sign(timestamp));
            logger.info("url {}", url.toString());
            HttpPost httpPost = new HttpPost(url.toString());
            httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
            ContentType contentType = ContentType.create("application/json", CharsetUtils.get("UTF-8"));
            DingDingMessage dingDingMessage = new DingDingMessage(message);
            httpPost.setEntity(new StringEntity(JSON.toJSONString(dingDingMessage), contentType));
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                if (null != entity) {
                    responseInfo = EntityUtils.toString(entity);
                }
            }
        } catch (Exception e) {
            logger.error("sendDingDingRobotMessage Exception {}", e.getMessage());
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                logger.error("sendDingDingRobotMessage IOException {}", e.getMessage());
            }
        }
        return responseInfo;
    }

    /**
     * 加签
     *
     * @param timestamp
     * @return
     */
    private String sign(Long timestamp) {
        String sign = "";
        try {
            String stringToSign = timestamp + "\n" + signSecret;
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(signSecret.getBytes("UTF-8"), "HmacSHA256"));
            byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
            sign = URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
            logger.info("sign {}", sign);
        } catch (Exception e) {
            logger.error("sendDingDingRobotMessage sign Exception {}", e.getMessage());
        }
        return sign;
    }

    @Data
    class DingDingMessage {
        private String msgtype = "text";
        private Map text = new HashMap();
        private Map at = new HashMap();

        DingDingMessage(String content) {
            text.put("content", content);
            at.put("isAtAll", false);
        }

        DingDingMessage(String content,Boolean isAtAll) {
            text.put("content", content);
            at.put("isAtAll", isAtAll);
        }
    }

    public static void main(String[] args) {
        String msg = new DingDingRobotUtil().sendDingDingRobotMessage("测试");
        System.out.println(msg);
    }
}