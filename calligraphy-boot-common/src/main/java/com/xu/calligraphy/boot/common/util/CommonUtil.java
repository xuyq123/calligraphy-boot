package com.xu.calligraphy.boot.common.util;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.xu.calligraphy.boot.common.CalligraphyBootException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xyq
 * @date 2021/7/23 14:20
 */
public class CommonUtil {

    private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);

    public static final BigDecimal hundred = BigDecimal.valueOf(100);

    public static Random random = new Random();
    private static Pattern PHONE_NUMBER_PATTERN;
    private static Pattern PHONE_NUMBER_PATTERN2;

    static {
        PHONE_NUMBER_PATTERN = Pattern.compile("^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$");
        PHONE_NUMBER_PATTERN2 = Pattern.compile("^[1][0-9]{10}$");
    }


    /**
     * 生成唯一性单号
     * 仓库首字母大写 + 时间（年月日时分秒毫秒值）+ 类型（回收-HS,配送-PS）
     * 如 HZCPS202008210924577854127
     *
     * @param warehouseName
     * @param date
     * @param type
     * @return
     */
    public static String assembleUniqueOrder(String warehouseName, Date date, String type) {
        StringBuffer buffer = new StringBuffer();
        try {
            Instant instant = date.toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
            String time = formatter.format(localDateTime);
            buffer.append(PingYinUtil.getFirstSpell(warehouseName).toUpperCase()).append(type).
                    append(time).append(CommonUtil.generateRandomCode(4));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isEmpty(buffer.toString())) {
            throw new CalligraphyBootException("生成唯一性单号异常");
        }
        return buffer.toString();
    }

    /**
     * 校验手机号
     *
     * @param phoneNumber
     * @return
     */
    public static boolean isPhoneNumber1(String phoneNumber) {
        if (StringUtils.isEmpty(phoneNumber) || phoneNumber.length() != 11) {
            return false;
        } else {
            Matcher m = PHONE_NUMBER_PATTERN.matcher(phoneNumber);
            return m.matches();
        }
    }

    public static boolean isPhoneNumber(String phoneNumber) {
        if (StringUtils.isEmpty(phoneNumber) || phoneNumber.length() != 11) {
            return false;
        } else {
            Matcher m = PHONE_NUMBER_PATTERN2.matcher(phoneNumber);
            return m.matches();
        }
    }

    /**
     * 生成随机数
     *
     * @param num
     * @return
     */
    public static String generateRandomCode(int num) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < num; i++) {
            stringBuffer.append(random.nextInt(10));
        }
        return stringBuffer.toString();
    }

    /**
     * 根据两个经纬度坐标计算距离（单位：千米）
     *
     * @param lon1
     * @param lat1
     * @param lon2
     * @param lat2
     * @return
     */
    public static double getDistance(double lon1, double lat1, double lon2, double lat2) {

        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);

        double a = radLat1 - radLat2;
        double b = rad(lon1) - rad(lon2);

        double c = 2 * Math.asin(Math.sqrt(
                Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math
                        .pow(Math.sin(b / 2), 2)));

        c = c * 6378.137;// 6378.137赤道半径

        return Math.round(c * 10000d) / 10000d;

    }

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    public static String listToStr(List<String> list) {

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < list.size(); i++) {

            sb.append("'").append(list.get(i)).append("'").append(",");

        }

        return sb.toString().substring(0, sb.length() - 1);

    }

    public static String getDatePoor(Date endDate, Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long hour = diff / nh;
        // 计算差多少小时
        long min = diff % nh / nm;
        return hour + "." + min + "h";
    }

    public static String getRandomStr(int length) {
        String base = "0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(assembleUniqueOrder("浙江仓", new Date(), "CS"));
        String pwd = "123456";
        System.out.println(md5Text(pwd));
        String encodePassword = encodePassword(pwd);
        System.out.println(checkEncryptionPassword(pwd, encodePassword));
        // checkEncryptionPassword  plaintextPassword=123456,encryptionPassword=$2y$10$WpgeBShjp345TcbmjZhSCuhxjBXsIMHh/X.fuhsvwOBSZjaFiSgNW
    }

    /**
     * hash加密
     *
     * @param plaintextPassword
     * @return
     */
    public static String encodePassword(String plaintextPassword) {
        return BCrypt.with(BCrypt.Version.VERSION_2Y).hashToString(10, plaintextPassword.toCharArray());
    }

    /**
     * 检查hash加密密码
     * java 实现 PHP password_hash() 密码验证
     * https://blog.csdn.net/danssion/article/details/89959720
     *
     * @param plaintextPassword  明文密码
     * @param encryptionPassword 加密密码
     * @return
     */
    public static boolean checkEncryptionPassword(String plaintextPassword, String encryptionPassword) {
        if (StringUtils.isEmpty(plaintextPassword) || StringUtils.isEmpty(encryptionPassword)) {
            return false;
        }
        logger.info("checkEncryptionPassword  plaintextPassword={},encryptionPassword={}", plaintextPassword, encryptionPassword);
        BCrypt.Result res = BCrypt.verifyer().verify(plaintextPassword.toCharArray(), encryptionPassword);
        return res.verified;
    }


    /**
     * md5加密
     *
     * @param plaintextPassword
     * @return
     */
    public static String md5Text(String plaintextPassword) {
        return DigestUtils.md5DigestAsHex(plaintextPassword.getBytes());
    }

}
