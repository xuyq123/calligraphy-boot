package com.xu.calligraphy.boot.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author xu
 * @date 2021/7/29 13:38
 */
public class DateUtil {

    /*** 时间格式化*/
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /*** 小时格式化*/
    public static final SimpleDateFormat SIMPLE_HOUR_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH");
    /*** 日期格式化*/
    public static final SimpleDateFormat SIMPLE_DAY = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SIMPLE_MONTH = new SimpleDateFormat("yyyy-MM");

    /**
     * 初始化时间
     *
     * @return
     */
    public static Date initDefaultDate() {
        try {
            return SIMPLE_DATE_FORMAT.parse("1111-11-11 11:11:11");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取明天日期
     *
     * @param date
     * @return
     */
    public static String getTomorrow(Date date) {
        return getDateString(date, 1);
    }

    /**
     * 获取指定日期
     *
     * @param date
     * @param num  整数是以后的日期，负数是之前的日期
     * @return
     */
    public static String getDateString(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        //把日期往后增加一天.整数往后推,负数往前移动
        calendar.add(Calendar.DATE, num);
        date = calendar.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    /**
     * 昨天
     *
     * @param date
     * @return yyyy-MM-dd
     */
    public static String getYesterday(Date date) {
        return getDateString(date, -1);
    }

    /**
     * 获取上个月
     *
     * @param date
     * @return yyyy-MM
     */
    public static String lastMonth(Date date) {
        Calendar c = Calendar.getInstance();
        Date now = c.getTime();
        c.setTime(now);
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DATE, 1);
        return SIMPLE_DAY.format(c.getTime()).substring(0, 7);
    }

    /**
     * 获取指定月份最后一天
     *
     * @param yearMonth 2019-02
     * @return yyyy-MM-dd
     */
    public static String getLastDayOfMonth(String yearMonth) {
        int year = Integer.parseInt(yearMonth.split("-")[0]);  //年
        int month = Integer.parseInt(yearMonth.split("-")[1]); //月
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR, year);
        // 设置月份
        // cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.MONTH, month); //设置当前月的上一个月
        // 获取某月最大天数
        //int lastDay = cal.getActualMaximum(Calendar.DATE);
        int lastDay = cal.getMinimum(Calendar.DATE); //获取月份中的最小值，即第一天
        // 设置日历中月份的最大天数
        //cal.set(Calendar.DAY_OF_MONTH, lastDay);
        cal.set(Calendar.DAY_OF_MONTH, lastDay - 1); //上月的第一天减去1就是当月的最后一天
        // 格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    /**
     * 指定时间
     *
     * @param date
     * @return yyyy-MM-dd
     */
    public static String getDate(Date date, Integer num) {
        return getDateString(date, num);
    }
}
