package com.xu.calligraphy.boot.service.impl;

import com.xu.calligraphy.boot.common.Page;
import com.xu.calligraphy.boot.common.Result;
import com.xu.calligraphy.boot.common.enums.UserSexEnum;
import com.xu.calligraphy.boot.common.util.CopyUtil;
import com.xu.calligraphy.boot.common.util.DingDingRobotUtil;
import com.xu.calligraphy.boot.common.util.HttpUtil;
import com.xu.calligraphy.boot.dal.mapper.UserDOMapper;
import com.xu.calligraphy.boot.dal.model.UserDO;
import com.xu.calligraphy.boot.dal.params.ActivityPublishParams;
import com.xu.calligraphy.boot.dal.query.UserQuery;
import com.xu.calligraphy.boot.service.UserService;
import com.xu.calligraphy.boot.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDOMapper userDOMapper;

    @Autowired
    private DingDingRobotUtil dingDingRobotUtil;

    @Autowired
    private HttpUtil httpUtil;

    @Override
    public String queryLoginName(Long id) {
        return "test";
    }

    @Override
    public Result selectById(Long id) {
        return Result.success(userDOMapper.selectByPrimaryKey(id));
    }

    @Override
    public Result queryUserList(UserQuery query) {
        Long count = userDOMapper.queryUserListCount(query);
        Result result = new Result();
        if (count > 0) {
            List<UserDO> list = userDOMapper.queryUserList(query);
            List<UserDTO> dtoList = CopyUtil.copyPropertiesList(list, UserDTO.class);
            dtoList.stream().forEach(userDTO -> {
                userDTO.setSexText(UserSexEnum.getSexTextByCode(userDTO.getSex()));
            });
            Page page = new Page(query.getCurrentPage(), count, dtoList);
            result.setContent(page);
        }
        return result;
    }

    @Override
    public Result sendDingMsg(ActivityPublishParams params) {
        if (!StringUtils.isEmpty(params.getContent())) {
            String text = String.format("%s \n--by %s", params.getContent(), httpUtil.getIpAddr());
            dingDingRobotUtil.sendDingDingRobotMessage(text);
            return Result.success();
        }
        return Result.error("content can not bu null");
    }
}
